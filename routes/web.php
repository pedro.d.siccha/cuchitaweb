<?php

use Illuminate\Support\Facades\Route;
use Kreait\Firebase\Factory;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    
    return view('web');
})->name('/');

Route::get('/tiendaVirtual', function(){

    return view('tiendaVirtual');
    
})->name('tiendaVirtual');

Route::get('/verProducto', function(){

    return view('vistaProducto');

})->name('verProducto');

Route::get('/pagarWeb', function(){

    return view('pagarWeb');

})->name('pagarWeb');

Route::get('/webvista', function () {

    $firebase = (new Factory)->withServiceAccount(__DIR__.'/firebaseService.json');

    $database = $firebase->createDatabase();

    $ref = $database->getReference("Negocio");

    $negocios = $ref->getValue();

        foreach ($negocios as $n) {

            $tiendas[] = $n;

        }

    return view('welcome', compact('tiendas'));
    
})->name('webvista');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::post('login/{provider}/callback', 'Auth\LoginController@handleCallback');


Route::get('/tienda', [App\Http\Controllers\TiendaController::class, 'index'])->name('tienda');
Route::get('/crearTienda', [App\Http\Controllers\TiendaController::class, 'crear'])->name('crearTienda');

Route::get('/administrarTienda', [App\Http\Controllers\TiendaController::class, 'administrarTienda'])->name('administrarTienda');
Route::get('/editarTienda', [App\Http\Controllers\TiendaController::class, 'editarTienda'])->name('editarTienda');

Route::get('/perfil', [App\Http\Controllers\AdministracionController::class, 'perfilUsuario'])->name('perfil');

Route::get('/anuncio', [App\Http\Controllers\AnuncioController::class, 'index'])->name('anuncio');

Route::get('/anuncioEstadistica', [App\Http\Controllers\AnuncioController::class, 'estadistica'])->name('anuncioEstadistica');

Route::get('/basica', [App\Http\Controllers\ConfiguracionesController::class, 'basica'])->name('basica');

Route::get('/general', [App\Http\Controllers\ConfiguracionesController::class, 'general'])->name('general');

Route::get('/misGanancias', [App\Http\Controllers\FinanzasController::class, 'misGanancias'])->name('misGanancias');

Route::get('/misMetas', [App\Http\Controllers\FinanzasController::class, 'misMetas'])->name('misMetas');

Route::get('/buzon', [App\Http\Controllers\MensajeriaController::class, 'buzon'])->name('buzon');

Route::get('/escribir', [App\Http\Controllers\MensajeriaController::class, 'escribir'])->name('escribir');