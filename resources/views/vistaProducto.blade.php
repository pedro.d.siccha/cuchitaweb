<!--
    Author: W3layouts
    Author URL: http://w3layouts.com
    License: Creative Commons Attribution 3.0 Unported
    License URL: http://creativecommons.org/licenses/by/3.0/
    -->
    <!DOCTYPE html>
    <html>
    <head>
        <title>CUCHITA WEB | PRODUCTO</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Smart Shop Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
    Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //for-mobile-apps -->
    <link href="{{ ('web/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="{{ ('web/css/flexslider.css') }}" type="text/css" media="screen" />
    <link href="{{ ('web/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
    <!-- js -->
    <script type="text/javascript" src="{{ ('web/js/jquery-2.1.4.min.js') }}"></script>
    <!-- //js -->
    <!-- single -->
    <script src="{{ ('web/js/imagezoom.js') }}"></script>
    <script src="{{ ('web/js/jquery.flexslider.js') }}"></script>
    <!-- single -->
    <!-- cart -->
        <script src="{{ ('web/js/simpleCart.min.js') }}"></script>
    <!-- cart -->
    <!-- for bootstrap working -->
        <script type="text/javascript" src="{{ ('web/js/bootstrap-3.1.1.min.js') }}"></script>
    <!-- //for bootstrap working -->
    <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,900,900italic,700italic' rel='stylesheet' type='text/css'>
    <script src="{{ ('web/js/jquery.easing.min.js') }}"></script>
    </head>
    <body>
    <!-- header -->
    <div class="header">
        <div class="container">
            <ul>
                <li><span class="glyphicon glyphicon-time" aria-hidden="true"></span>Delivery Rápido</li>
                <li><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>Compra todo lo que necesitas</li>
                <li><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><a href="mailto:cuchita@gmail.com">cuchita@gmail.com</a></li>
            </ul>
        </div>
    </div>
    <!-- //header -->
    <!-- header-bot -->
    <div class="header-bot">
        <div class="container">
            <div class="col-md-3 header-left">
                <h1><a href="{{ Route('webvista') }}"><img src="{{ asset('web/images/logo.png') }}" style="width: 35%" height="35%"></a></h1>
            </div>
            <div class="col-md-6 header-middle">
                <form>
                    <div class="search">
                        <input type="search" value="Buscar" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
                    </div>
                    <div class="section_room">
                        <select id="country" onchange="change_country(this.value)" class="frm-field required">
                            <option value="null">Todas las categorías</option>
                            <option value="null">Comidas y Bebidas</option>     
                            <option value="AX">Supermercados/Tiendas</option>
                            <option value="AX">Juguetes y Regalos</option>
                            <option value="AX">Mascotas</option>
                            <option value="AX">PC y Gaming</option>
                            <option value="AX">Farmacia</option>
                            <option value="AX">Ferrtería</option>
                            <option value="AX">Moda y Accesorios</option>
                        </select>
                    </div>
                    <div class="sear-sub">
                        <input type="submit" value=" ">
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
            <div class="col-md-3 header-right footer-bottom">
                <ul>
                    <li><a href="#" class="use1" data-toggle="modal" data-target="#myModal4"><span>Ingresar</span></a></li>
                    <li><a class="fb" href="#"></a></li>
                    <li><a class="insta" href="#"></a></li>
                    <li><a class="cong" href="{{ Route('login') }}"></a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- //header-bot -->
    <!-- banner -->
    <div class="ban-top">
        <div class="container">
            <div class="top_nav_left">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav menu__list">
                        <li class="active menu__item menu__item--current"><a class="menu__link" href="{{ Route('webvista') }}">Inicio <span class="sr-only">(current)</span></a></li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Comidas <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-6 multi-gd-img1 multi-gd-text ">
                                            <a href="#"><img src="{{ asset('web/images/materialEstatico/catComidaBebida.png') }}" alt=" " style="height: 50%"/></a>
                                        </div>
                                        <div class="col-sm-6 multi-gd-img">
                                            <ul class="multi-column-dropdown">
                                                <li><a href="mens.html">Comida 1</a></li>
                                                <li><a href="mens.html">Comida 2</a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tiendas <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-6 multi-gd-img">
                                            <ul class="multi-column-dropdown">
                                            </ul>
                                        </div>
                                        <div class="col-sm-6 multi-gd-img multi-gd-text ">
                                            <a href="womens.html"><img src="{{ asset('web/images/materialEstatico/catMarkets.png') }}" alt=" "/></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Regalos <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-6 multi-gd-img1 multi-gd-text ">
                                            <a href="mens.html"><img src="{{ asset('web/images/materialEstatico/regalos.png') }}" alt=" " style="height: 50%"/></a>
                                        </div>
                                        <div class="col-sm-6 multi-gd-img">
                                            <ul class="multi-column-dropdown">

                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mascotas <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-6 multi-gd-img">
                                            <ul class="multi-column-dropdown">
                                                
                                            </ul>
                                        </div>
                                        <div class="col-sm-6 multi-gd-img multi-gd-text ">
                                            <a href="womens.html"><img src="{{ asset('web/images/materialEstatico/catMascota.png') }}" alt=" "/></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PC <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-6 multi-gd-img multi-gd-text ">
                                            <a href="womens.html"><img src="{{ asset('web/images/materialEstatico/catPc.png') }}" alt=" "/></a>
                                        </div>
                                        <div class="col-sm-6 multi-gd-img">
                                            <ul class="multi-column-dropdown">
                                                <li><a href="{{ Route('tiendaVirtual') }}">Inforad</a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Farmacia <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-6 multi-gd-img">
                                            <ul class="multi-column-dropdown">
                                                
                                            </ul>
                                        </div>
                                        <div class="col-sm-6 multi-gd-img multi-gd-text ">
                                            <a href="womens.html"><img src="{{ asset('web/images/materialEstatico/catFarmacia.png') }}" alt=" "/></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                      </ul>
                    </div>
                  </div>
                </nav>	
            </div>
            <div class="top_nav_right">
                <div class="cart box_1">
                            <a href="{{ Route('pagarWeb') }}">
                                <h3> <div class="total">
                                    <i class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></i>
                                    <span class="simpleCart_total"></span> (<span id="simpleCart_quantity" class="simpleCart_quantity"></span> items)</div>
                                    
                                </h3>
                            </a>
                            <p><a href="javascript:;" class="simpleCart_empty">Carrito de Compras</a></p>
                            
                </div>	
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- //banner-top -->
    <!-- single -->
    <div class="single">
        <div class="container">
            <div class="col-md-6 single-right-left animated wow slideInUp animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInUp;">
                <div class="grid images_3_of_2">
                    <div class="flexslider">
                        <!-- FlexSlider -->
                            <script>
                            // Can also be used with $(document).ready()
                                $(window).load(function() {
                                    $('.flexslider').flexslider({
                                    animation: "slide",
                                    controlNav: "thumbnails"
                                    });
                                });
                            </script>
                        <!-- //FlexSlider-->
                        <ul class="slides">
                            <li data-thumb="{{ ('web/images/d2.jpg') }}">
                                <div class="thumb-image"> <img src="{{ ('web/images/d2.jpg') }}" data-imagezoom="true" class="img-responsive"> </div>
                            </li>
                            <li data-thumb="{{ ('web/images/d1.jpg') }}">
                                <div class="thumb-image"> <img src="{{ ('web/images/d1.jpg') }}" data-imagezoom="true" class="img-responsive"> </div>
                            </li>	
                            <li data-thumb="{{ ('web/images/d3.jpg') }}">
                                <div class="thumb-image"> <img src="{{ ('web/images/d3.jpg') }}" data-imagezoom="true" class="img-responsive"> </div>
                            </li>
                            <li data-thumb="{{ ('web/images/d4.jpg') }}">
                                <div class="thumb-image"> <img src="{{ ('web/images/d4.jpg') }}" data-imagezoom="true" class="img-responsive"> </div>
                            </li>	
                        </ul>
                        <div class="clearfix"></div>
                    </div>	
                </div>
            </div>
            <div class="col-md-6 single-right-left simpleCart_shelfItem animated wow slideInRight animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInRight;">
                        <h3>Producto 01</h3>
                        <p><span class="item_price">S/. 550</span> <del>- S/. 900</del></p>
                        <div class="rating1">
                            <span class="starRating">
                                <input id="rating5" type="radio" name="rating" value="5">
                                <label for="rating5">5</label>
                                <input id="rating4" type="radio" name="rating" value="4">
                                <label for="rating4">4</label>
                                <input id="rating3" type="radio" name="rating" value="3" checked="">
                                <label for="rating3">3</label>
                                <input id="rating2" type="radio" name="rating" value="2">
                                <label for="rating2">2</label>
                                <input id="rating1" type="radio" name="rating" value="1">
                                <label for="rating1">1</label>
                            </span>
                        </div>
                        <div class="description">
                            <h5>Descripción del producto 1</h5>
                            <input type="text" value="Ingresar Cupón" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter pincode';}" required="">
                            <input type="submit" value="Check">
                        </div>
                        <div class="color-quality">
                            <div class="color-quality-right">
                                <h5>Talla :</h5>
                                <select id="country1" onchange="change_country(this.value)" class="frm-field required sect">
                                    <option value="null">35</option>
                                    <option value="null">36</option> 
                                    <option value="null">37</option>					
                                    <option value="null">40</option>								
                                </select>
                            </div>
                        </div>
                        <div class="occasional">
                            <h5>Tipos :</h5>
                            <div class="colr ert">
                                <label class="radio"><input type="radio" name="radio" checked=""><i></i>Zapato Casual</label>
                            </div>
                            <div class="colr">
                                <label class="radio"><input type="radio" name="radio"><i></i>Zapato Deportivo</label>
                            </div>
                            <div class="colr">
                                <label class="radio"><input type="radio" name="radio"><i></i>Zapato Formal</label>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                        <div class="occasion-cart">
                            <a href="#" class="item_add hvr-outline-out button2">AGREGAR</a>
                        </div>
                        
            </div>
                    <div class="clearfix"> </div>
    
                    <div class="bootstrap-tab animated wow slideInUp animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInUp;">
                        <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                            <ul id="myTab" class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Descripción</a></li>
                                <li role="presentation"><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">Comentarios(1)</a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active bootstrap-tab-text" id="home" aria-labelledby="home-tab">
                                    <h5>Nombre del Producto</h5>
                                    <p>Descripcion del producto <span>Producto de prueba.</span></p>
                                </div>
                                <div role="tabpanel" class="tab-pane fade bootstrap-tab-text" id="profile" aria-labelledby="profile-tab">
                                    <div class="bootstrap-tab-text-grids">
                                        <div class="bootstrap-tab-text-grid">
                                            <div class="bootstrap-tab-text-grid-left">
                                                <img src="{{ ('web/images/men3.jpg') }}" alt=" " class="img-responsive">
                                            </div>
                                            <div class="bootstrap-tab-text-grid-right">
                                                <ul>
                                                    <li><a href="#">Admin</a></li>
                                                    <li><a href="#"><span class="glyphicon glyphicon-share" aria-hidden="true"></span>Responder</a></li>
                                                </ul>
                                                <p>Comentario 1.</p>
                                            </div>
                                            <div class="clearfix"> </div>
                                        </div>
                                        
                                        <div class="add-review">
                                            <h4>Agregar Comentario</h4>
                                            <form>
                                                <input type="text" value="Usuario" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Usuario';}" required="">
                                                <input type="email" value="Correo" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Correo';}" required="">
                                                
                                                <textarea type="text" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message...';}" required="">Comentario...</textarea>
                                                <input type="submit" value="ENVIAR">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade bootstrap-tab-text" id="dropdown1" aria-labelledby="dropdown1-tab">
                                    <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p>
                                </div>
                                <div role="tabpanel" class="tab-pane fade bootstrap-tab-text" id="dropdown2" aria-labelledby="dropdown2-tab">
                                    <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
    </div>
    <!-- //single -->
    <!-- //product-nav -->
    <div class="coupons">
        <div class="container">
            <div class="coupons-grids text-center">
                <div class="col-md-3 coupons-gd">
                    <h3>Compra tus productos de forma sencilla</h3>
                </div>
                <div class="col-md-3 coupons-gd">
                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                    <h4>INGRESE A SU CUENTA</h4>
                    <p>Para poder obtener más promociones.</p>
                </div>
                <div class="col-md-3 coupons-gd">
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                    <h4>SELECCIONA TU ARTICULO</h4>
                    <p>Tenemos articulos de diferentes tiendas y rubros.</p>
                </div>
                <div class="col-md-3 coupons-gd">
                    <span class="glyphicon glyphicon-credit-card" aria-hidden="true"></span>
                    <h4>HACER EL PAGO</h4>
                    <p>Paga por todos tus articulos de diferentes tiendas y rubros con un solo botón.</p>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!-- footer -->
    <div class="footer">
        <div class="container">
            <div class="col-md-3 footer-left">
                <h2><a href="{{ Route('webvista') }}"><img src="{{ asset('web/images/logo.png') }}" alt=" " /></a></h2>
                <p>Cuchita web es un sistema multi-tienda donde podrá encontrar todos los articulos que busca y podrá solicitarlos hasta la comodidad de su hogar.</p>
            </div>
            <div class="col-md-9 footer-right">
                <div class="col-sm-6 newsleft">
                    <h3>REGISTRESÉ PARA MÁS OFERTAS !</h3>
                </div>
                <div class="col-sm-6 newsright">
                    <form>
                        <input type="text" value="Correo" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                        <input type="submit" value="Submit">
                    </form>
                </div>
                <div class="clearfix"></div>
                <div class="sign-grds">
                    <div class="col-md-4 sign-gd">
                        <h4>Información</h4>
                        <ul>
                            <li><a href="{{ Route('webvista') }}">INICIO</a></li>
                            <li><a href="mens.html">COMIDA</a></li>
                            <li><a href="womens.html">TIENDAS</a></li>
                            <li><a href="electronics.html">REGALOS</a></li>
                            <li><a href="codes.html">MASCOTAS</a></li>
                            <li><a href="contact.html">PC</a></li>
                            <li><a href="contact.html">FARMACIA</a></li>
                        </ul>
                    </div>
                    <!--
                    <div class="col-md-4 sign-gd-two">
                        <h4>Store Information</h4>
                        <ul>
                            <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Address : 1234k Avenue, 4th block, <span>Newyork City.</span></li>
                            <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email : <a href="mailto:info@example.com">info@example.com</a></li>
                            <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Phone : +1234 567 567</li>
                        </ul>
                    </div>
                    -->
                    <div class="col-md-4 sign-gd flickr-post">
                        <h4>Publicaciones</h4>
                        <ul>
                            <li><a href="single.html"><img src="{{ asset('web/images/b15.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b16.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b17.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b18.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b15.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b16.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b17.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b18.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b15.jpg') }}" alt=" " class="img-responsive" /></a></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
            <p class="copy-right">&copy 2021 Cuchita Web. Derechos reservados | Desarrollado por <a href="https://inforad.com.pe/">inforad.com.pe</a></p>
        </div>
    </div>
    <!-- //footer -->
    <!-- login -->
                <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content modal-info">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
                            </div>
                            <div class="modal-body modal-spa">
                                <div class="login-grids">
                                    <div class="login">
                                        <div class="login-bottom">
                                            <h3>Sign up for free</h3>
                                            <form>
                                                <div class="sign-up">
                                                    <h4>Email :</h4>
                                                    <input type="text" value="Type here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}" required="">	
                                                </div>
                                                <div class="sign-up">
                                                    <h4>Password :</h4>
                                                    <input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
                                                    
                                                </div>
                                                <div class="sign-up">
                                                    <h4>Re-type Password :</h4>
                                                    <input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
                                                    
                                                </div>
                                                <div class="sign-up">
                                                    <input type="submit" value="REGISTER NOW" >
                                                </div>
                                                
                                            </form>
                                        </div>
                                        <div class="login-right">
                                            <h3>Sign in with your account</h3>
                                            <form>
                                                <div class="sign-in">
                                                    <h4>Email :</h4>
                                                    <input type="text" value="Type here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}" required="">	
                                                </div>
                                                <div class="sign-in">
                                                    <h4>Password :</h4>
                                                    <input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
                                                    <a href="#">Forgot password?</a>
                                                </div>
                                                <div class="single-bottom">
                                                    <input type="checkbox"  id="brand" value="">
                                                    <label for="brand"><span></span>Remember Me.</label>
                                                </div>
                                                <div class="sign-in">
                                                    <input type="submit" value="SIGNIN" >
                                                </div>
                                            </form>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <p>By logging in you agree to our <a href="#">Terms and Conditions</a> and <a href="#">Privacy Policy</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    <!-- //login -->
    </body>
    </html>
    