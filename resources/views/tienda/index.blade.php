@extends('layouts.app')
@section('contenido')
<!-- Content Header (Tiendas) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Oficinas</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active"><a href="{{ Route('tienda') }}" class="text-dark">Tiendas</a></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
  <div class="container-fluid">

    <table class="table table-hover text-nowrap">
      <thead>
        <tr>
          <th>#</th>
          <th>Imagen</th>
          <th>Oficinas</th>
          <th>Creación</th>
          <th>Estado</th>
          <th>Categoía</th>
          <th>Administrar</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($tiendas as $t)
        <tr>
          <td>{{ $t["id"] }}</td>
          <td>
            <img src="{{ $t["imagen"] }}" alt="{{ $t["nombre"] }}" style="height: 10%;">
          </td>
          <td>{{ $t["nombre"] }}</td>
          <td>05-01-2021</td>
          <td>Activo</td>
          <td>{{ $t["categoria"] }}</td>
          <td>
            <a class="btn btn-app bg-secondary" href="{{ Route('administrarTienda') }}">
              <i class="fas fa-cogs"></i> Administrar
            </a>
            <a class="btn btn-app bg-warning" href="{{ Route('editarTienda') }}">
              <i class="fas fa-pencil-alt"></i> Editar
            </a>
            <a class="btn btn-app bg-danger" onclick="eliminarTienda()">
              <i class="fas fa-trash"></i> Eliminar
            </a>
          </td>
        </tr>    
        @endforeach
      </tbody>
    </table>

  </div>
</section>

@endsection
@section('script')

<script>

  function eliminarTienda(){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: 'Desea Eliminar la tienda?',
      text: "Seguro que desea eliminar la TIENDA 1!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        swalWithBootstrapButtons.fire(
          'Eliminado!',
          'Su tienda fue eliminada correctamente.',
          'success'
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'Continúe trabajando :)',
          'error'
        )
      }
    }) 
  }

</script>
  
    
@endsection