<!--
    Author: W3layouts
    Author URL: http://w3layouts.com
    License: Creative Commons Attribution 3.0 Unported
    License URL: http://creativecommons.org/licenses/by/3.0/
    -->
    <!DOCTYPE html>
    <html>
    <head>
        <title>CUCHITA WEB | CARRITO</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Smart Shop Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
    Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //for-mobile-apps -->
    <link href="{{ ('web/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ ('web/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
    <!-- js -->
    <script type="text/javascript" src="{{ ('web/js/jquery-2.1.4.min.js') }}"></script>
    <!-- //js -->
    <!-- cart -->
        <script src="{{ ('web/js/simpleCart.min.js') }}"></script>
    <!-- cart -->
    <!-- for bootstrap working -->
        <script type="text/javascript" src="{{ ('web/js/bootstrap-3.1.1.min.js') }}"></script>
    <!-- //for bootstrap working -->
    <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,900,900italic,700italic' rel='stylesheet' type='text/css'>
    <script src="{{ ('web/js/jquery.easing.min.js') }}"></script>
    </head>
    <body>
    <!-- header -->
    <div class="header">
        <div class="container">
            <ul>
                <li><span class="glyphicon glyphicon-time" aria-hidden="true"></span>Delivery Rápido</li>
                <li><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>Compra todo lo que necesitas</li>
                <li><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><a href="mailto:cuchita@gmail.com">cuchita@gmail.com</a></li>
            </ul>
        </div>
    </div>
    <!-- //header -->
    <!-- header-bot -->
    <div class="header-bot">
        <div class="container">
            <div class="col-md-3 header-left">
                <h1><a href="{{ Route('/') }}"><img src="{{ asset('web/images/logo.png') }}" style="width: 35%" height="35%"></a></h1>
            </div>
            <div class="col-md-6 header-middle">
                <form>
                    <div class="search">
                        <input type="search" value="Buscar" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
                    </div>
                    <div class="section_room">
                        <select id="country" onchange="change_country(this.value)" class="frm-field required">
                            <option value="null">Todas las categorías</option>
                            <option value="null">Comidas y Bebidas</option>     
                            <option value="AX">Supermercados/Tiendas</option>
                            <option value="AX">Juguetes y Regalos</option>
                            <option value="AX">Mascotas</option>
                            <option value="AX">PC y Gaming</option>
                            <option value="AX">Farmacia</option>
                            <option value="AX">Ferrtería</option>
                            <option value="AX">Moda y Accesorios</option>
                        </select>
                    </div>
                    <div class="sear-sub">
                        <input type="submit" value=" ">
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
            <div class="col-md-3 header-right footer-bottom">
                <ul>
                    <li><a href="#" class="use1" data-toggle="modal" data-target="#myModal4"><span>Ingresar</span></a></li>
                    <li><a class="fb" href="#"></a></li>
                    <li><a class="insta" href="#"></a></li>
                    <li><a class="cong" href="{{ Route('login') }}"></a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- //header-bot -->
    <!-- banner -->
    <div class="ban-top">
        <div class="container">
            <div class="top_nav_left">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav menu__list">
                        <li class="active menu__item menu__item--current"><a class="menu__link" href="{{ Route('webvista') }}">Inicio <span class="sr-only">(current)</span></a></li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Comidas <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-6 multi-gd-img1 multi-gd-text ">
                                            <a href="#"><img src="{{ asset('web/images/materialEstatico/catComidaBebida.png') }}" alt=" " style="height: 50%"/></a>
                                        </div>
                                        <div class="col-sm-6 multi-gd-img">
                                            <ul class="multi-column-dropdown">
                                                <li><a href="mens.html">Comida 1</a></li>
                                                <li><a href="mens.html">Comida 2</a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tiendas <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-6 multi-gd-img">
                                            <ul class="multi-column-dropdown">
                                            </ul>
                                        </div>
                                        <div class="col-sm-6 multi-gd-img multi-gd-text ">
                                            <a href="womens.html"><img src="{{ asset('web/images/materialEstatico/catMarkets.png') }}" alt=" "/></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Regalos <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-6 multi-gd-img1 multi-gd-text ">
                                            <a href="mens.html"><img src="{{ asset('web/images/materialEstatico/regalos.png') }}" alt=" " style="height: 50%"/></a>
                                        </div>
                                        <div class="col-sm-6 multi-gd-img">
                                            <ul class="multi-column-dropdown">

                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mascotas <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-6 multi-gd-img">
                                            <ul class="multi-column-dropdown">
                                                
                                            </ul>
                                        </div>
                                        <div class="col-sm-6 multi-gd-img multi-gd-text ">
                                            <a href="womens.html"><img src="{{ asset('web/images/materialEstatico/catMascota.png') }}" alt=" "/></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PC <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-6 multi-gd-img multi-gd-text ">
                                            <a href="womens.html"><img src="{{ asset('web/images/materialEstatico/catPc.png') }}" alt=" "/></a>
                                        </div>
                                        <div class="col-sm-6 multi-gd-img">
                                            <ul class="multi-column-dropdown">
                                                <li><a href="{{ Route('tiendaVirtual') }}">Inforad</a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Farmacia <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-6 multi-gd-img">
                                            <ul class="multi-column-dropdown">
                                                
                                            </ul>
                                        </div>
                                        <div class="col-sm-6 multi-gd-img multi-gd-text ">
                                            <a href="womens.html"><img src="{{ asset('web/images/materialEstatico/catFarmacia.png') }}" alt=" "/></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                      </ul>
                    </div>
                  </div>
                </nav>	
            </div>
            <div class="top_nav_right">
                <div class="cart box_1">
                            <a href="{{ Route('pagarWeb') }}">
                                <h3> <div class="total">
                                    <i class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></i>
                                    <span class="simpleCart_total"></span> (<span id="simpleCart_quantity" class="simpleCart_quantity"></span> items)</div>
                                    
                                </h3>
                            </a>
                            <p><a href="javascript:;" class="simpleCart_empty">Carrito de Compras</a></p>
                            
                </div>	
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- //banner-top -->
    <!-- banner -->
    <!-- check out -->
    <div class="checkout">
        <div class="container">
            <h3>Mi Carrito de Compras</h3>
            <div class="table-responsive checkout-right animated wow slideInUp" data-wow-delay=".5s">
                <table class="timetable_sub">
                    <thead>
                        <tr>
                            <th>Quitar</th>
                            <th>Img</th>
                            <th>Cantidad</th>
                            <th>Producto</th>
                            <th>Precio</th>
                        </tr>
                    </thead>
                        <tr class="rem1">
                            <td class="invert-closeb">
                                <div class="rem">
                                    <div class="close1"> </div>
                                </div>
                                <script>$(document).ready(function(c) {
                                    $('.close1').on('click', function(c){
                                        $('.rem1').fadeOut('slow', function(c){
                                            $('.rem1').remove();
                                        });
                                        });	  
                                    });
                               </script>
                            </td>
                            <td class="invert-image"><a href="single.html"><img src="{{ ('web/images/w4.png') }}" alt=" " class="img-responsive" /></a></td>
                            <td class="invert">
                                 <div class="quantity"> 
                                    <div class="quantity-select">                           
                                        <div class="entry value-minus">&nbsp;</div>
                                        <div class="entry value"><span>1</span></div>
                                        <div class="entry value-plus active">&nbsp;</div>
                                    </div>
                                </div>
                            </td>
                            <td class="invert">Bolsa de Mano</td>
                            <td class="invert">S/. 45.99</td>
                        </tr>
                        <tr class="rem2">
                            <td class="invert-closeb">
                                <div class="rem">
                                    <div class="close2"> </div>
                                </div>
                                <script>$(document).ready(function(c) {
                                    $('.close2').on('click', function(c){
                                        $('.rem2').fadeOut('slow', function(c){
                                            $('.rem2').remove();
                                        });
                                        });	  
                                    });
                               </script>
                            </td>
                            <td class="invert-image"><a href="single.html"><img src="{{ ('web/images/ep3.png') }}" alt=" " class="img-responsive" /></a></td>
                            <td class="invert">
                                 <div class="quantity"> 
                                    <div class="quantity-select">                           
                                        <div class="entry value-minus">&nbsp;</div>
                                        <div class="entry value"><span>1</span></div>
                                        <div class="entry value-plus active">&nbsp;</div>
                                    </div>
                                </div>
                            </td>
                            <td class="invert">Reloj</td>
                            <td class="invert">S/. 45.99</td>
                            
                        </tr>
                        <tr class="rem3">
                            <td class="invert-closeb">
                                <div class="rem">
                                    <div class="close3"> </div>
                                </div>
                                <script>$(document).ready(function(c) {
                                    $('.close3').on('click', function(c){
                                        $('.rem3').fadeOut('slow', function(c){
                                            $('.rem3').remove();
                                        });
                                        });	  
                                    });
                               </script>
                            </td>
                            <td class="invert-image"><a href="single.html"><img src="{{ ('web/images/w2.png') }}" alt=" " class="img-responsive" /></a></td>
                            <td class="invert">
                                 <div class="quantity"> 
                                    <div class="quantity-select">                           
                                        <div class="entry value-minus">&nbsp;</div>
                                        <div class="entry value"><span>1</span></div>
                                        <div class="entry value-plus active">&nbsp;</div>
                                    </div>
                                </div>
                            </td>
                            <td class="invert">Sandalias</td>
                            <td class="invert">S/. 45.99</td>
                            
                        </tr>
                        <tr class="rem4">
                            <td class="invert-closeb">
                                <div class="rem">
                                    <div class="close4"> </div>
                                </div>
                                <script>$(document).ready(function(c) {
                                    $('.close4').on('click', function(c){
                                        $('.rem4').fadeOut('slow', function(c){
                                            $('.rem4').remove();
                                        });
                                        });	  
                                    });
                               </script>
                            </td>
                            <td class="invert-image"><a href="single.html"><img src="{{ ('web/images/w1.png') }}" alt=" " class="img-responsive" /></a></td>
                            <td class="invert">
                                 <div class="quantity"> 
                                    <div class="quantity-select">                           
                                        <div class="entry value-minus">&nbsp;</div>
                                        <div class="entry value"><span>1</span></div>
                                        <div class="entry value-plus active">&nbsp;</div>
                                    </div>
                                </div>
                            </td>
                            <td class="invert">Tacones</td>
                            <td class="invert">S/. 45.99</td>
                            
                        </tr>
                        
                                    <!--quantity-->
                                        <script>
                                        $('.value-plus').on('click', function(){
                                            var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10)+1;
                                            divUpd.text(newVal);
                                        });
    
                                        $('.value-minus').on('click', function(){
                                            var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10)-1;
                                            if(newVal>=1) divUpd.text(newVal);
                                        });
                                        </script>
                                    <!--quantity-->
                </table>
            </div>
            <div class="checkout-left">	
                    
                    <div class="checkout-right-basket animated wow slideInRight" data-wow-delay=".5s">
                        <a href="mens.html"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>Seguir Comprando</a>
                    </div>
                    <div class="checkout-left-basket animated wow slideInLeft" data-wow-delay=".5s">
                        <h4>Resumen de Compra</h4>
                        <ul>
                            <li>Bolsa de Mano <i>-</i> <span>S/. 45.99</span></li>
                            <li>Reloj <i>-</i> <span>S/. 45.99</span></li>
                            <li>Sandalias <i>-</i> <span>S/. 45.99</span></li>
                            <li>Tacones <i>-</i> <span>S/. 45.99</span></li>
                            <li>Total <i>-</i> <span>S/. 183.96</span></li>
                        </ul>
                    </div>
                    <div class="clearfix"> </div>
                </div>
        </div>
    </div>	
    <!-- //check out -->
    <!-- //product-nav -->
    <div class="coupons">
        <div class="container">
            <div class="coupons-grids text-center">
                <div class="col-md-3 coupons-gd">
                    <h3>Compra tus productos de forma sencilla</h3>
                </div>
                <div class="col-md-3 coupons-gd">
                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                    <h4>INGRESE A SU CUENTA</h4>
                    <p>Para poder obtener más promociones.</p>
                </div>
                <div class="col-md-3 coupons-gd">
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                    <h4>SELECCIONA TU ARTICULO</h4>
                    <p>Tenemos articulos de diferentes tiendas y rubros.</p>
                </div>
                <div class="col-md-3 coupons-gd">
                    <span class="glyphicon glyphicon-credit-card" aria-hidden="true"></span>
                    <h4>HACER EL PAGO</h4>
                    <p>Paga por todos tus articulos de diferentes tiendas y rubros con un solo botón.</p>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!-- footer -->
    <div class="footer">
        <div class="container">
            <div class="col-md-3 footer-left">
                <h2><a href="{{ Route('webvista') }}"><img src="{{ asset('web/images/logo.png') }}" alt=" " /></a></h2>
                <p>Cuchita web es un sistema multi-tienda donde podrá encontrar todos los articulos que busca y podrá solicitarlos hasta la comodidad de su hogar.</p>
            </div>
            <div class="col-md-9 footer-right">
                <div class="col-sm-6 newsleft">
                    <h3>REGISTRESÉ PARA MÁS OFERTAS !</h3>
                </div>
                <div class="col-sm-6 newsright">
                    <form>
                        <input type="text" value="Correo" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                        <input type="submit" value="Submit">
                    </form>
                </div>
                <div class="clearfix"></div>
                <div class="sign-grds">
                    <div class="col-md-4 sign-gd">
                        <h4>Información</h4>
                        <ul>
                            <li><a href="{{ Route('webvista') }}">INICIO</a></li>
                            <li><a href="mens.html">COMIDA</a></li>
                            <li><a href="womens.html">TIENDAS</a></li>
                            <li><a href="electronics.html">REGALOS</a></li>
                            <li><a href="codes.html">MASCOTAS</a></li>
                            <li><a href="contact.html">PC</a></li>
                            <li><a href="contact.html">FARMACIA</a></li>
                        </ul>
                    </div>
                    <!--
                    <div class="col-md-4 sign-gd-two">
                        <h4>Store Information</h4>
                        <ul>
                            <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Address : 1234k Avenue, 4th block, <span>Newyork City.</span></li>
                            <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email : <a href="mailto:info@example.com">info@example.com</a></li>
                            <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Phone : +1234 567 567</li>
                        </ul>
                    </div>
                    -->
                    <div class="col-md-4 sign-gd flickr-post">
                        <h4>Publicaciones</h4>
                        <ul>
                            <li><a href="single.html"><img src="{{ asset('web/images/b15.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b16.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b17.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b18.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b15.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b16.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b17.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b18.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b15.jpg') }}" alt=" " class="img-responsive" /></a></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
            <p class="copy-right">&copy 2021 Cuchita Web. Derechos reservados | Desarrollado por <a href="https://inforad.com.pe/">inforad.com.pe</a></p>
        </div>
    </div>
    <!-- //footer -->
    <!-- login -->
                <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content modal-info">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
                            </div>
                            <div class="modal-body modal-spa">
                                <div class="login-grids">
                                    <div class="login">
                                        <div class="login-bottom">
                                            <h3>Sign up for free</h3>
                                            <form>
                                                <div class="sign-up">
                                                    <h4>Email :</h4>
                                                    <input type="text" value="Type here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}" required="">	
                                                </div>
                                                <div class="sign-up">
                                                    <h4>Password :</h4>
                                                    <input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
                                                    
                                                </div>
                                                <div class="sign-up">
                                                    <h4>Re-type Password :</h4>
                                                    <input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
                                                    
                                                </div>
                                                <div class="sign-up">
                                                    <input type="submit" value="REGISTER NOW" >
                                                </div>
                                                
                                            </form>
                                        </div>
                                        <div class="login-right">
                                            <h3>Sign in with your account</h3>
                                            <form>
                                                <div class="sign-in">
                                                    <h4>Email :</h4>
                                                    <input type="text" value="Type here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}" required="">	
                                                </div>
                                                <div class="sign-in">
                                                    <h4>Password :</h4>
                                                    <input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
                                                    <a href="#">Forgot password?</a>
                                                </div>
                                                <div class="single-bottom">
                                                    <input type="checkbox"  id="brand" value="">
                                                    <label for="brand"><span></span>Remember Me.</label>
                                                </div>
                                                <div class="sign-in">
                                                    <input type="submit" value="SIGNIN" >
                                                </div>
                                            </form>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <p>By logging in you agree to our <a href="#">Terms and Conditions</a> and <a href="#">Privacy Policy</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    <!-- //login -->
    </body>
    </html>
    