<!--
    Author: W3layouts
    Author URL: http://w3layouts.com
    License: Creative Commons Attribution 3.0 Unported
    License URL: http://creativecommons.org/licenses/by/3.0/
    -->
    <!DOCTYPE html>
    <html>
    <head>
    <title>CUCHITA WEB | Inicio</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Smart Shop Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //for-mobile-apps -->
    <link href="{{ asset('web/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
    <!-- pignose css -->
    <link href="{{ asset('web/css/pignose.layerslider.css') }}" rel="stylesheet" type="text/css" media="all" />
    
    
    <!-- //pignose css -->
    <link href="{{ asset('web/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
    <!-- js -->
    <script type="text/javascript" src="{{ asset('web/js/jquery-2.1.4.min.js') }}"></script>
    <!-- //js -->
    <!-- cart -->
        <script src="{{ asset('web/js/simpleCart.min.js') }}"></script>
    <!-- cart -->
    <!-- for bootstrap working -->
        <script type="text/javascript" src="{{ asset('web/js/bootstrap-3.1.1.min.js') }}"></script>
    <!-- //for bootstrap working -->
    <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,900,900italic,700italic' rel='stylesheet' type='text/css'>
    <script src="{{ asset('web/js/jquery.easing.min.js') }}"></script>
    </head>
    <body>
    <!-- header -->
    <div class="header">
        <div class="container">
            <ul>
                <li><span class="glyphicon glyphicon-time" aria-hidden="true"></span>Delivery Rápido</li>
                <li><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>Compra todo lo que necesitas</li>
                <li><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><a href="mailto:cuchita@gmail.com">cuchita@gmail.com</a></li>
            </ul>
        </div>
    </div>
    <!-- //header -->
    <!-- header-bot -->
    <div class="header-bot">
        <div class="container">
            <div class="col-md-3 header-left">
                <h1><a href="{{ Route('webvista') }}"><img src="{{ asset('web/images/logo.png') }}" style="width: 35%" height="35%"></a></h1>
            </div>
            <div class="col-md-6 header-middle">
                <form>
                    <div class="search">
                        <input type="search" value="Buscar" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
                    </div>
                    <div class="section_room">
                        <select id="country" onchange="change_country(this.value)" class="frm-field required">
                            <option value="null">Todas las categorías</option>
                            <option value="null">Comidas y Bebidas</option>     
                            <option value="AX">Supermercados/Tiendas</option>
                            <option value="AX">Juguetes y Regalos</option>
                            <option value="AX">Mascotas</option>
                            <option value="AX">PC y Gaming</option>
                            <option value="AX">Farmacia</option>
                            <option value="AX">Ferrtería</option>
                            <option value="AX">Moda y Accesorios</option>
                        </select>
                    </div>
                    <div class="sear-sub">
                        <input type="submit" value=" ">
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
            <div class="col-md-3 header-right footer-bottom">
                <ul>
                    <li><a href="#" class="use1" data-toggle="modal" data-target="#myModal4"><span>Ingresar</span></a></li>
                    <li><a class="fb" href="#"></a></li>
                    <li><a class="insta" href="#"></a></li>
                    <li><a class="cong" href="{{ Route('login') }}"></a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- //header-bot -->
    <!-- banner -->
    <div class="ban-top">
        <div class="container">
            <div class="top_nav_left">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav menu__list">
                        <li class="active menu__item menu__item--current"><a class="menu__link" href="{{ Route('/') }}">Inicio <span class="sr-only">(current)</span></a></li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Comidas <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-6 multi-gd-img1 multi-gd-text ">
                                            <a href="#"><img src="{{ asset('web/images/materialEstatico/catComidaBebida.png') }}" alt=" " style="height: 50%"/></a>
                                        </div>
                                        <div class="col-sm-6 multi-gd-img">
                                            <ul class="multi-column-dropdown">
                                                <li><a href="mens.html">Comida 1</a></li>
                                                <li><a href="mens.html">Comida 2</a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tiendas <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-6 multi-gd-img">
                                            <ul class="multi-column-dropdown">
                                            </ul>
                                        </div>
                                        <div class="col-sm-6 multi-gd-img multi-gd-text ">
                                            <a href="womens.html"><img src="{{ asset('web/images/materialEstatico/catMarkets.png') }}" alt=" "/></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Regalos <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-6 multi-gd-img1 multi-gd-text ">
                                            <a href="mens.html"><img src="{{ asset('web/images/materialEstatico/regalos.png') }}" alt=" " style="height: 50%"/></a>
                                        </div>
                                        <div class="col-sm-6 multi-gd-img">
                                            <ul class="multi-column-dropdown">

                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mascotas <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-6 multi-gd-img">
                                            <ul class="multi-column-dropdown">
                                                
                                            </ul>
                                        </div>
                                        <div class="col-sm-6 multi-gd-img multi-gd-text ">
                                            <a href="womens.html"><img src="{{ asset('web/images/materialEstatico/catMascota.png') }}" alt=" "/></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PC <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-6 multi-gd-img multi-gd-text ">
                                            <a href="womens.html"><img src="{{ asset('web/images/materialEstatico/catPc.png') }}" alt=" "/></a>
                                        </div>
                                        <div class="col-sm-6 multi-gd-img">
                                            <ul class="multi-column-dropdown">
                                                <li><a href="{{ Route('tiendaVirtual') }}">Inforad</a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Farmacia <span class="caret"></span></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-6 multi-gd-img">
                                            <ul class="multi-column-dropdown">
                                                
                                            </ul>
                                        </div>
                                        <div class="col-sm-6 multi-gd-img multi-gd-text ">
                                            <a href="womens.html"><img src="{{ asset('web/images/materialEstatico/catFarmacia.png') }}" alt=" "/></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                        </li>
                      </ul>
                    </div>
                  </div>
                </nav>	
            </div>
            <div class="top_nav_right">
                <div class="cart box_1">
                            <a href="{{ Route('pagarWeb') }}">
                                <h3> <div class="total">
                                    <i class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></i>
                                    <span class="simpleCart_total"></span> (<span id="simpleCart_quantity" class="simpleCart_quantity"></span> items)</div>
                                    
                                </h3>
                            </a>
                            <p><a href="javascript:;" class="simpleCart_empty">Carrito de Compras</a></p>
                            
                </div>	
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- //banner-top -->
    <!-- banner -->
    <div class="banner-grid">
        <div id="visual">
                <div class="slide-visual">
                    <!-- Slide Image Area (1000 x 424) -->
                    <ul class="slide-group">
                        <li><img class="img-responsive" src="{{ asset('web/images/ba1.jpg') }}" alt="Dummy Image" /></li>
                        <li><img class="img-responsive" src="{{ asset('web/images/ba2.jpg') }}" alt="Dummy Image" /></li>
                        <li><img class="img-responsive" src="{{ asset('web/images/ba3.jpg') }}" alt="Dummy Image" /></li>
                    </ul>
    
                    <!-- Slide Description Image Area (316 x 328) -->
                    <div class="script-wrap">
                        <ul class="script-group">
                            <li><div class="inner-script"><img class="img-responsive" src="{{ asset('web/images/baa1.jpg') }}" alt="Dummy Image" /></div></li>
                            <li><div class="inner-script"><img class="img-responsive" src="{{ asset('web/images/baa2.jpg') }}" alt="Dummy Image" /></div></li>
                            <li><div class="inner-script"><img class="img-responsive" src="{{ asset('web/images/baa3.jpg') }}" alt="Dummy Image" /></div></li>
                        </ul>
                        <div class="slide-controller">
                            <a href="#" class="btn-prev"><img src="{{ asset('web/images/btn_prev.png') }}" alt="Prev Slide" /></a>
                            <a href="#" class="btn-play"><img src="{{ asset('web/images/btn_play.png') }}" alt="Start Slide" /></a>
                            <a href="#" class="btn-pause"><img src="{{ asset('web/images/btn_pause.png') }}" alt="Pause Slide" /></a>
                            <a href="#" class="btn-next"><img src="{{ asset('web/images/btn_next.png') }}" alt="Next Slide" /></a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        <script type="text/javascript" src="{{ asset('web/js/pignose.layerslider.js') }}"></script>
        <script type="text/javascript">
        //<![CDATA[
            $(window).load(function() {
                $('#visual').pignoseLayerSlider({
                    play    : '.btn-play',
                    pause   : '.btn-pause',
                    next    : '.btn-next',
                    prev    : '.btn-prev'
                });
            });
        //]]>
        </script>
    
    </div>
    <!-- //banner -->
    <!-- content -->
    
    <div class="new_arrivals">
        <div class="container">
            <h3><span>Nuevos </span>productos</h3>
            <p>Nuevos productos en vitrina</p>
            <div class="new_grids">
                <div class="col-md-4 new-gd-left">
                    <img src="{{ asset('web/images/wed1.jpg') }}" alt=" " />
                    <div class="wed-brand simpleCart_shelfItem">
                        <h4>Producto 1</h4>
                        <h5>50% de Descuento</h5>
                        <p><i>S/. 250</i> <span class="item_price">S/. 500</span><a class="item_add hvr-outline-out button2" href="#">comprar </a></p>
                    </div>
                </div>
                <div class="col-md-4 new-gd-middle">
                    <div class="new-levis">
                        <div class="mid-img">
                            <img src="{{ asset('web/images/levis1.png') }}" alt=" " />
                        </div>
                        <div class="mid-text">
                            <h4>Descuento <span>40%</span></h4>
                            <a class="hvr-outline-out button2" href="{{ Route('tiendaVirtual') }}">VER MÁS </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="new-levis">
                        <div class="mid-text">
                            <h4>Descuento <span>50%</span></h4>
                            <a class="hvr-outline-out button2" href="{{ Route('tiendaVirtual') }}">Shop now </a>
                        </div>
                        <div class="mid-img">
                            <img src="{{ asset('web/images/dig.jpg') }}" alt=" " />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-4 new-gd-left">
                    <img src="{{ asset('web/images/wed2.jpg') }}" alt=" " />
                    <div class="wed-brandtwo simpleCart_shelfItem">
                        <h4>Nuevas Promociones</h4>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- //content -->
    
    <!-- content-bottom -->
    
    <div class="content-bottom">
        <div class="col-md-7 content-lgrid">
            <div class="col-sm-6 content-img-left text-center">
                <div class="content-grid-effect slow-zoom vertical">
                    <div class="img-box"><img src="{{ asset('web/images/p1.jpg') }}" alt="image" class="img-responsive zoom-img"></div>
                        <div class="info-box">
                            <div class="info-content simpleCart_shelfItem">
                                        <h4>Mobiles</h4>
                                        <span class="separator"></span>
                                        <p><span class="item_price">S/. 500</span></p>
                                        <span class="separator"></span>
                                        <a class="item_add hvr-outline-out button2" href="#">AGREGAR </a>
                            </div>
                        </div>
                </div>
            </div>
            <div class="col-sm-6 content-img-right">
                <h3>Ofertas Especiales y 50%<span>Descuento</span> Mobiles</h3>
            </div>
            
            <div class="col-sm-6 content-img-right">
                <h3>Cupones <span> En </span> INFORAD</h3>
            </div>
            <div class="col-sm-6 content-img-left text-center">
                <div class="content-grid-effect slow-zoom vertical">
                    <div class="img-box"><img src="{{ asset('web/images/p2.jpg') }}" alt="image" class="img-responsive zoom-img"></div>
                        <div class="info-box">
                            <div class="info-content simpleCart_shelfItem">
                                <h4>Relojes</h4>
                                <span class="separator"></span>
                                <p><span class="item_price">S/. 250</span></p>
                                <span class="separator"></span>
                                <a class="item_add hvr-outline-out button2" href="#">AGREGAR </a>
                            </div>
                        </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-5 content-rgrid text-center">
            <div class="content-grid-effect slow-zoom vertical">
                    <div class="img-box"><img src="{{ asset('web/images/p4.jpg') }}" alt="image" class="img-responsive zoom-img"></div>
                        <div class="info-box">
                            <div class="info-content simpleCart_shelfItem">
                                        <h4>Zapatos</h4>
                                        <span class="separator"></span>
                                        <p><span class="item_price">S/. 150</span></p>
                                        <span class="separator"></span>
                                        <a class="item_add hvr-outline-out button2" href="#">AGREGAR </a>
                            </div>
                        </div>
                </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- //content-bottom -->
    <!-- product-nav -->
    
    <div class="product-easy">
        <div class="container">
            
            <script src="{{ ('web/js/easyResponsiveTabs.js') }}" type="text/javascript"></script>
            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#horizontalTab').easyResponsiveTabs({
                                        type: 'default', //Types: default, vertical, accordion           
                                        width: 'auto', //auto or any width like 600px
                                        fit: true   // 100% fit in a container
                                    });
                                });
                                
            </script>
            <div class="sap_tabs">
                <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                    <ul class="resp-tabs-list">
                        <li class="resp-tab-item" aria-controls="tab_item-0" role="tab"><span>Populares</span></li> 
                        <li class="resp-tab-item" aria-controls="tab_item-1" role="tab"><span>Nuevas Ofertas</span></li> 
                        <li class="resp-tab-item" aria-controls="tab_item-2" role="tab"><span>Productos Nuevos</span></li> 
                    </ul>				  	 
                    <div class="resp-tabs-container">
                        <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
                            <div class="col-md-3 product-men">
                                <div class="men-pro-item simpleCart_shelfItem">
                                    <div class="men-thumb-item">
                                        <img src="{{ asset('web/images/a1.png') }}" alt="" class="pro-image-front">
                                        <img src="{{ asset('web/images/a1.png') }}" alt="" class="pro-image-back">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="{{ Route('verProducto') }}" class="link-product-add-cart">Ver Más</a>
                                                </div>
                                            </div>
                                            <span class="product-new-top">Nuevo</span>
                                            
                                    </div>
                                    <div class="item-info-product ">
                                        <h4><a href="{{ Route('verProducto') }}">Polo</a></h4>
                                        <div class="info-product-price">
                                            <span class="item_price">S/. 45.99</span>
                                            <del>S/. 69.71</del>
                                        </div>
                                        <a href="#" class="item_add single-item hvr-outline-out button2">AGREGAR</a>									
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-men">
                                <div class="men-pro-item simpleCart_shelfItem">
                                    <div class="men-thumb-item">
                                        <img src="{{ asset('web/images/a8.png') }}" alt="" class="pro-image-front">
                                        <img src="{{ asset('web/images/a8.png') }}" alt="" class="pro-image-back">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="{{ Route('verProducto') }}" class="link-product-add-cart">Ver Más</a>
                                                </div>
                                            </div>
                                            <span class="product-new-top">Cupón</span>
                                            
                                    </div>
                                    <div class="item-info-product ">
                                        <h4><a href="{{ Route('verProducto') }}">Blazer</a></h4>
                                        <div class="info-product-price">
                                            <span class="item_price">S/. 99.99</span>
                                            <del>S/. 109.71</del>
                                        </div>
                                        <a href="#" class="item_add single-item hvr-outline-out button2">AGREGAR</a>									
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-men">
                                <div class="men-pro-item simpleCart_shelfItem">
                                    <div class="men-thumb-item">
                                        <img src="{{ asset('web/images/a3.png') }}" alt="" class="pro-image-front">
                                        <img src="{{ asset('web/images/a3.png') }}" alt="" class="pro-image-back">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="{{ Route('verProducto') }}" class="link-product-add-cart">Ver Más</a>
                                                </div>
                                            </div>
                                            <span class="product-new-top">Nuevo</span>
                                            
                                    </div>
                                    <div class="item-info-product ">
                                        <h4><a href="{{ Route('verProducto') }}">Polo Negro </a></h4>
                                        <div class="info-product-price">
                                            <span class="item_price">S/. 119.99</span>
                                            <del>S/. 120.71</del>
                                        </div>
                                        <a href="#" class="item_add single-item hvr-outline-out button2">AGREGAR</a>									
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-men">
                                <div class="men-pro-item simpleCart_shelfItem">
                                    <div class="men-thumb-item">
                                        <img src="{{ asset('web/images/a4.png') }}" alt="" class="pro-image-front">
                                        <img src="{{ asset('web/images/a4.png') }}" alt="" class="pro-image-back">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="{{ Route('verProducto') }}" class="link-product-add-cart">Ver Más</a>
                                                </div>
                                            </div>
                                            <span class="product-new-top">Nuevo</span>
                                            
                                    </div>
                                    <div class="item-info-product ">
                                        <h4><a href="single.html">Polo Marrón</a></h4>
                                        <div class="info-product-price">
                                            <span class="item_price">S/. 79.99</span>
                                            <del>S/. 120.71</del>
                                        </div>
                                        <a href="#" class="item_add single-item hvr-outline-out button2">AGREGAR</a>									
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-men yes-marg">
                                <div class="men-pro-item simpleCart_shelfItem">
                                    <div class="men-thumb-item">
                                        <img src="{{ asset('web/images/a5.png') }}" alt="" class="pro-image-front">
                                        <img src="{{ asset('web/images/a5.png') }}" alt="" class="pro-image-back">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="{{ Route('verProducto') }}" class="link-product-add-cart">Ver Más</a>
                                                </div>
                                            </div>
                                            <span class="product-new-top">Oferta</span>
                                            
                                    </div>
                                    <div class="item-info-product ">
                                        <h4><a href="single.html">Polo</a></h4>
                                        <div class="info-product-price">
                                            <span class="item_price">S/. 129.99</span>
                                            <del>S/. 150.71</del>
                                        </div>
                                        <a href="#" class="item_add single-item hvr-outline-out button2">AGREGAR</a>									
                                    </div>
                                </div>
                            </div>
                            
                            <div class="clearfix"></div>
                        </div>
                        <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-1">
                            <div class="col-md-3 product-men">
                                <div class="men-pro-item simpleCart_shelfItem">
                                    <div class="men-thumb-item">
                                        <img src="{{ asset('web/images/w1.png') }}" alt="" class="pro-image-front">
                                        <img src="{{ asset('web/images/w1.png') }}" alt="" class="pro-image-back">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="single.html" class="link-product-add-cart">Quick View</a>
                                                </div>
                                            </div>
                                            <span class="product-new-top">New</span>
                                            
                                    </div>
                                    <div class="item-info-product ">
                                        <h4><a href="single.html">Wedges</a></h4>
                                        <div class="info-product-price">
                                            <span class="item_price">$45.99</span>
                                            <del>$69.71</del>
                                        </div>
                                        <a href="#" class="item_add single-item hvr-outline-out button2">Add to cart</a>									
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-men">
                                <div class="men-pro-item simpleCart_shelfItem">
                                    <div class="men-thumb-item">
                                        <img src="{{ asset('web/images/w2.png') }}" alt="" class="pro-image-front">
                                        <img src="{{ asset('web/images/w2.png') }}" alt="" class="pro-image-back">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="single.html" class="link-product-add-cart">Quick View</a>
                                                </div>
                                            </div>
                                            <span class="product-new-top">New</span>
                                            
                                    </div>
                                    <div class="item-info-product ">
                                        <h4><a href="single.html">Sandals</a></h4>
                                        <div class="info-product-price">
                                            <span class="item_price">$45.99</span>
                                            <del>$69.71</del>
                                        </div>
                                        <a href="#" class="item_add single-item hvr-outline-out button2">Add to cart</a>									
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-men">
                                <div class="men-pro-item simpleCart_shelfItem">
                                    <div class="men-thumb-item">
                                        <img src="{{ asset('web/images/mw1.png') }}" alt="" class="pro-image-front">
                                        <img src="{{ asset('web/images/mw1.png') }}" alt="" class="pro-image-back">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="single.html" class="link-product-add-cart">Quick View</a>
                                                </div>
                                            </div>
                                            <span class="product-new-top">New</span>
                                            
                                    </div>
                                    <div class="item-info-product ">
                                        <h4><a href="single.html">Casual Shoes</a></h4>
                                        <div class="info-product-price">
                                            <span class="item_price">$45.99</span>
                                            <del>$69.71</del>
                                        </div>
                                        <a href="#" class="item_add single-item hvr-outline-out button2">Add to cart</a>									
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-men">
                                <div class="men-pro-item simpleCart_shelfItem">
                                    <div class="men-thumb-item">
                                        <img src="{{ asset('web/images/mw3.png') }}" alt="" class="pro-image-front">
                                        <img src="{{ asset('web/images/mw3.png') }}" alt="" class="pro-image-back">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="single.html" class="link-product-add-cart">Quick View</a>
                                                </div>
                                            </div>
                                            <span class="product-new-top">New</span>
                                            
                                    </div>
                                    <div class="item-info-product ">
                                        <h4><a href="single.html">Sport Shoes</a></h4>
                                        <div class="info-product-price">
                                            <span class="item_price">$45.99</span>
                                            <del>$69.71</del>
                                        </div>
                                        <a href="#" class="item_add single-item hvr-outline-out button2">Add to cart</a>									
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-men yes-marg">
                                <div class="men-pro-item simpleCart_shelfItem">
                                    <div class="men-thumb-item">
                                        <img src="{{ asset('web/images/ep2.png') }}" alt="" class="pro-image-front">
                                        <img src="{{ asset('web/images/ep2.png') }}" alt="" class="pro-image-back">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="single.html" class="link-product-add-cart">Quick View</a>
                                                </div>
                                            </div>
                                            <span class="product-new-top">New</span>
                                            
                                    </div>
                                    <div class="item-info-product ">
                                        <h4><a href="single.html">Watches</a></h4>
                                        <div class="info-product-price">
                                            <span class="item_price">$45.99</span>
                                            <del>$69.71</del>
                                        </div>
                                        <a href="#" class="item_add single-item hvr-outline-out button2">Add to cart</a>									
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-men yes-marg">
                                <div class="men-pro-item simpleCart_shelfItem">
                                    <div class="men-thumb-item">
                                        <img src="{{ asset('web/images/ep3.png') }}" alt="" class="pro-image-front">
                                        <img src="{{ asset('web/images/ep3.png') }}" alt="" class="pro-image-back">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="single.html" class="link-product-add-cart">Quick View</a>
                                                </div>
                                            </div>
                                            <span class="product-new-top">New</span>
                                            
                                    </div>
                                    <div class="item-info-product ">
                                        <h4><a href="single.html">Watches</a></h4>
                                        <div class="info-product-price">
                                            <span class="item_price">$45.99</span>
                                            <del>$69.71</del>
                                        </div>
                                        <a href="#" class="item_add single-item hvr-outline-out button2">Add to cart</a>									
                                    </div>
                                </div>
                            </div>
                            
                            <div class="clearfix"></div>						
                        </div>
                        <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-2">
                            <div class="col-md-3 product-men">
                                <div class="men-pro-item simpleCart_shelfItem">
                                    <div class="men-thumb-item">
                                        <img src="{{ asset('web/images/g1.png') }}" alt="" class="pro-image-front">
                                        <img src="{{ asset('web/images/g1.png') }}" alt="" class="pro-image-back">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="single.html" class="link-product-add-cart">Quick View</a>
                                                </div>
                                            </div>
                                            <span class="product-new-top">New</span>
                                            
                                    </div>
                                    <div class="item-info-product ">
                                        <h4><a href="single.html">Dresses</a></h4>
                                        <div class="info-product-price">
                                            <span class="item_price">$45.99</span>
                                            <del>$69.71</del>
                                        </div>
                                        <a href="#" class="item_add single-item hvr-outline-out button2">Add to cart</a>									
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-men">
                                <div class="men-pro-item simpleCart_shelfItem">
                                    <div class="men-thumb-item">
                                        <img src="{{ asset('web/images/g2.png') }}" alt="" class="pro-image-front">
                                        <img src="{{ asset('web/images/g2.png') }}" alt="" class="pro-image-back">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="single.html" class="link-product-add-cart">Quick View</a>
                                                </div>
                                            </div>
                                            <span class="product-new-top">New</span>
                                            
                                    </div>
                                    <div class="item-info-product ">
                                        <h4><a href="single.html"> Shirts</a></h4>
                                        <div class="info-product-price">
                                            <span class="item_price">$45.99</span>
                                            <del>$69.71</del>
                                        </div>
                                        <a href="#" class="item_add single-item hvr-outline-out button2">Add to cart</a>									
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-men">
                                <div class="men-pro-item simpleCart_shelfItem">
                                    <div class="men-thumb-item">
                                        <img src="{{ asset('web/images/g3.png') }}" alt="" class="pro-image-front">
                                        <img src="{{ asset('web/images/g3.png') }}" alt="" class="pro-image-back">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="single.html" class="link-product-add-cart">Quick View</a>
                                                </div>
                                            </div>
                                            <span class="product-new-top">New</span>
                                            
                                    </div>
                                    <div class="item-info-product ">
                                        <h4><a href="single.html">Shirts</a></h4>
                                        <div class="info-product-price">
                                            <span class="item_price">$45.99</span>
                                            <del>$69.71</del>
                                        </div>
                                        <a href="#" class="item_add single-item hvr-outline-out button2">Add to cart</a>									
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-men">
                                <div class="men-pro-item simpleCart_shelfItem">
                                    <div class="men-thumb-item">
                                        <img src="{{ asset('web/images/mw2.png') }}" alt="" class="pro-image-front">
                                        <img src="{{ asset('web/images/mw2.png') }}" alt="" class="pro-image-back">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="single.html" class="link-product-add-cart">Quick View</a>
                                                </div>
                                            </div>
                                            <span class="product-new-top">New</span>
                                            
                                    </div>
                                    <div class="item-info-product ">
                                        <h4><a href="single.html">T shirts</a></h4>
                                        <div class="info-product-price">
                                            <span class="item_price">$45.99</span>
                                            <del>$69.71</del>
                                        </div>
                                        <a href="#" class="item_add single-item hvr-outline-out button2">Add to cart</a>									
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-men yes-marg">
                                <div class="men-pro-item simpleCart_shelfItem">
                                    <div class="men-thumb-item">
                                        <img src="{{ asset('web/images/w4.png') }}" alt="" class="pro-image-front">
                                        <img src="{{ asset('web/images/w4.png') }}" alt="" class="pro-image-back">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="single.html" class="link-product-add-cart">Quick View</a>
                                                </div>
                                            </div>
                                            <span class="product-new-top">New</span>
                                            
                                    </div>
                                    <div class="item-info-product ">
                                        <h4><a href="single.html">Air Tshirt Black Domyos</a></h4>
                                        <div class="info-product-price">
                                            <span class="item_price">$45.99</span>
                                            <del>$69.71</del>
                                        </div>
                                        <a href="#" class="item_add single-item hvr-outline-out button2">Add to cart</a>									
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-men yes-marg">
                                <div class="men-pro-item simpleCart_shelfItem">
                                    <div class="men-thumb-item">
                                        <img src="{{ asset('web/images/w3.png') }}" alt="" class="pro-image-front">
                                        <img src="{{ asset('web/images/w3.png') }}" alt="" class="pro-image-back">
                                            <div class="men-cart-pro">
                                                <div class="inner-men-cart-pro">
                                                    <a href="single.html" class="link-product-add-cart">Quick View</a>
                                                </div>
                                            </div>
                                            <span class="product-new-top">New</span>
                                            
                                    </div>
                                    <div class="item-info-product ">
                                        <h4><a href="single.html">Hand Bags</a></h4>
                                        <div class="info-product-price">
                                            <span class="item_price">$45.99</span>
                                            <del>$69.71</del>
                                        </div>
                                        <a href="#" class="item_add single-item hvr-outline-out button2">Add to cart</a>									
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>		
                        </div>	
                    </div>	
                </div>
            </div>
        </div>
    </div>
    <!-- //product-nav -->
    
    <div class="coupons">
        <div class="container">
            <div class="coupons-grids text-center">
                <div class="col-md-3 coupons-gd">
                    <h3>Compra tus productos de forma sencilla</h3>
                </div>
                <div class="col-md-3 coupons-gd">
                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                    <h4>INGRESE A SU CUENTA</h4>
                    <p>Para poder obtener más promociones.</p>
                </div>
                <div class="col-md-3 coupons-gd">
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                    <h4>SELECCIONA TU ARTICULO</h4>
                    <p>Tenemos articulos de diferentes tiendas y rubros.</p>
                </div>
                <div class="col-md-3 coupons-gd">
                    <span class="glyphicon glyphicon-credit-card" aria-hidden="true"></span>
                    <h4>HACER EL PAGO</h4>
                    <p>Paga por todos tus articulos de diferentes tiendas y rubros con un solo botón.</p>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!-- footer -->
    <div class="footer">
        <div class="container">
            <div class="col-md-3 footer-left">
                <h2><a href="{{ Route('webvista') }}"><img src="{{ asset('web/images/logo.png') }}" alt=" " /></a></h2>
                <p>Cuchita web es un sistema multi-tienda donde podrá encontrar todos los articulos que busca y podrá solicitarlos hasta la comodidad de su hogar.</p>
            </div>
            <div class="col-md-9 footer-right">
                <div class="col-sm-6 newsleft">
                    <h3>REGISTRESÉ PARA MÁS OFERTAS !</h3>
                </div>
                <div class="col-sm-6 newsright">
                    <form>
                        <input type="text" value="Correo" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                        <input type="submit" value="Submit">
                    </form>
                </div>
                <div class="clearfix"></div>
                <div class="sign-grds">
                    <div class="col-md-4 sign-gd">
                        <h4>Información</h4>
                        <ul>
                            <li><a href="{{ Route('webvista') }}">INICIO</a></li>
                            <li><a href="mens.html">COMIDA</a></li>
                            <li><a href="womens.html">TIENDAS</a></li>
                            <li><a href="electronics.html">REGALOS</a></li>
                            <li><a href="codes.html">MASCOTAS</a></li>
                            <li><a href="contact.html">PC</a></li>
                            <li><a href="contact.html">FARMACIA</a></li>
                        </ul>
                    </div>
                    <!--
                    <div class="col-md-4 sign-gd-two">
                        <h4>Store Information</h4>
                        <ul>
                            <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Address : 1234k Avenue, 4th block, <span>Newyork City.</span></li>
                            <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email : <a href="mailto:info@example.com">info@example.com</a></li>
                            <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Phone : +1234 567 567</li>
                        </ul>
                    </div>
                    -->
                    <div class="col-md-4 sign-gd flickr-post">
                        <h4>Publicaciones</h4>
                        <ul>
                            <li><a href="single.html"><img src="{{ asset('web/images/b15.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b16.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b17.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b18.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b15.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b16.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b17.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b18.jpg') }}" alt=" " class="img-responsive" /></a></li>
                            <li><a href="single.html"><img src="{{ asset('web/images/b15.jpg') }}" alt=" " class="img-responsive" /></a></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
            <p class="copy-right">&copy 2021 Cuchita Web. Derechos reservados | Desarrollado por <a href="https://inforad.com.pe/">inforad.com.pe</a></p>
        </div>
    </div>
    <!-- //footer -->
    <!-- login -->
                <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content modal-info">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
                            </div>
                            <div class="modal-body modal-spa">
                                <div class="login-grids">
                                    <div class="login">
                                        <div class="login-bottom">
                                            <h3>Registrese Gratis</h3>
                                            <form method="POST" action="{{ route('register') }}">
                                                @csrf
                                                <div class="sign-up">
                                                    <h4>Usuario :</h4>
                                                    <input placeholder="Usuario" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                                    @error('name')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="sign-up">
                                                    <h4>Correo :</h4>
                                                    <input placeholder="Correo" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                                    @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="sign-up">
                                                    <h4>Contraseña :</h4>
                                                    <input placeholder="Contraseña" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                                    @error('password')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                    
                                                </div>
                                                <div class="sign-up">
                                                    <h4>Confirmar Contraseña :</h4>
                                                    <input placeholder="Confirmar Contraseña" id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                                    
                                                </div>
                                                <div class="sign-up">
                                                    <input type="submit" value="Registrar" >
                                                </div>
                                                
                                            </form>
                                        </div>
                                        <div class="login-right">
                                            <h3>Iniciar Sesión</h3>
                                            <form method="POST" action="{{ route('login') }}">
                                                @csrf
                                                <div class="sign-in">
                                                    <h4>Correo :</h4>
                                                    <input placeholder="Correo" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="sign-in">
                                                    <h4>Contraseña :</h4>
                                                    <input placeholder="Contraseña" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                                    @error('password')
                                                        <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                    <a href="#">¿Olvidaste tu contraseña?</a>
                                                </div>
                                                <div class="single-bottom">
                                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                    <label for="brand"><span></span>Recordarme.</label>
                                                </div>
                                                <div class="sign-in">
                                                    <input type="submit" value="Ingresar" >
                                                </div>
                                            </form>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <p>Al iniciar sesión, aceptas nuestros <a href="#">términos y condiciones</a> y <a href="#">Políticas de Privacidad</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    <!-- //login -->
    <script src="{{ asset('./js/fb_crud.js') }}"></script>
    </body>
    </html>
    