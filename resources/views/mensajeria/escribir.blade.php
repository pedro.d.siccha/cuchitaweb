@extends('layouts.app')
@section('contenido')
<!-- Content Header (Escribir) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Escribir</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ Route('home') }}" class="text-dark">Inicio</a></li>
            <li class="breadcrumb-item active">Mensajería</li>
            <li class="breadcrumb-item active"><a href="{{ Route('tienda') }}" class="text-dark">Tiendas</a></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">
        <a href="mailbox.html" class="btn btn-primary btn-block mb-3">Volver al Buzón</a>
    
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Carpetas</h3>
    
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body p-0">
              <ul class="nav nav-pills flex-column">
                <li class="nav-item active">
                  <a href="#" class="nav-link">
                    <i class="fas fa-inbox"></i> Buzón
                    <span class="badge bg-primary float-right">12</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    <i class="far fa-envelope"></i> Enviados
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    <i class="far fa-trash-alt"></i> Eliminados
                  </a>
                </li>
              </ul>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
      </div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="card card-primary card-outline">
          <div class="card-header">
            <h3 class="card-title">Redactar Correo</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="form-group">
              <input class="form-control" placeholder="Para:">
            </div>
            <div class="form-group">
              <input class="form-control" placeholder="Asunto:">
            </div>
            <div class="form-group">
                <textarea id="compose-textarea" class="form-control" style="height: 300px">
                </textarea>
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <div class="float-right">
              <button type="submit" class="btn btn-primary"><i class="far fa-envelope"></i> Enviar</button>
            </div>
            <button type="reset" class="btn btn-default"><i class="fas fa-times"></i> Eliminar</button>
          </div>
          <!-- /.card-footer -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>

@endsection
@section('script')
  <script src="{{ asset('../../plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ asset('../../plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('../../dist/js/adminlte.min.js') }}"></script>
  <!-- Summernote -->
  <script src="{{ asset('../../plugins/summernote/summernote-bs4.min.js') }}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ asset('../../dist/js/demo.js') }}"></script>
  <!-- Page specific script -->
  <script>
    $(function () {
      //Add text editor
      $('#compose-textarea').summernote()
    })
  </script>
@endsection