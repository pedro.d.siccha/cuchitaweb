@extends('layouts.app')
@section('contenido')
<!-- Content Header (Básicas) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Configuraciones Básicas</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ Route('home') }}" class="text-dark">Inicio</a></li>
            <li class="breadcrumb-item">Configuraciones</li>
            <li class="breadcrumb-item active"><a href="{{ Route('basica') }}" class="text-dark">Básicas</a></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
@endsection
@section('script')
    
@endsection