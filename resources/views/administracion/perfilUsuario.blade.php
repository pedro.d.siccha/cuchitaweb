@extends('layouts.app')
@section('contenido')
<!-- Content Header (Perfil de Usuario) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Perfil de Usuario</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ Route('home') }}" class="text-dark">Inicio</a></li>
            <li class="breadcrumb-item">Administración</li>
            <li class="breadcrumb-item active"><a href="{{ Route('perfil') }}" class="text-dark">Perfil de Usuario</a></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <!-- Profile Image -->
        <div class="card card-primary card-outline">
          <div class="card-body box-profile">
            <div class="text-center">
              <img class="profile-user-img img-fluid img-circle"
                   src="../../dist/img/user4-128x128.jpg"
                   alt="User profile picture">
            </div>

            <h3 class="profile-username text-center">Administración</h3>

            <p class="text-muted text-center">Administrador</p>

            <ul class="list-group list-group-unbordered mb-3">
              <li class="list-group-item">
                <b>Tiendas</b> <a class="float-right">2</a>
              </li>
              <li class="list-group-item">
                <b>Contactos</b> <a class="float-right">43</a>
              </li>
            </ul>

            <a href="#" class="btn btn-primary btn-block"><b>Ver Contactos</b></a>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

        <!-- About Me Box -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Sobre Mi</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <strong><i class="fas fa-map-marker-alt mr-1"></i> Dirección</strong>

            <p class="text-muted">Huaraz, Independencia</p>

          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="card">
          <div class="card-header p-2">
            <ul class="nav nav-pills">
              <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Tiendas</a></li>
              <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Pedidos</a></li>
              <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Configuraciones</a></li>
            </ul>
          </div><!-- /.card-header -->
          <div class="card-body">
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                
                <div class="info-box mb-3 bg-success">
                  <span class="info-box-icon"><i class="fas fa-store-alt"></i></span>
    
                  <div class="info-box-content">
                    <span class="info-box-text">Tienda 1</span>
                    <span class="info-box-number">50</span>
                  </div>
                  <!-- /.info-box-content -->
                </div>

                <div class="info-box mb-3 bg-success">
                  <span class="info-box-icon"><i class="fas fa-store-alt"></i></span>
    
                  <div class="info-box-content">
                    <span class="info-box-text">Tienda 2</span>
                    <span class="info-box-number">5</span>
                  </div>
                  <!-- /.info-box-content -->
                </div>

              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="timeline">
                
                <div class="card-body p-0">
                  <div class="table-responsive">
                    <table class="table m-0">
                      <thead>
                      <tr>
                        <th>Cod. Orden</th>
                        <th>Cliente</th>
                        <th>Estado</th>
                        <th>Gestión</th>
                      </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><a href="pages/examples/invoice.html">OR002</a></td>
                          <td>Rad</td>
                          <td><span class="badge badge-warning">Pendiente</span></td>
                          <td>
                            <a class="btn btn-app bg-info">
                              <i class="fas fa-list"></i> Ver
                            </a>
                            <a class="btn btn-app bg-success">
                              <i class="fas fa-check"></i> Entregar
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td><a href="pages/examples/invoice.html">OR001</a></td>
                          <td>Rad</td>
                          <td><span class="badge badge-success">Entregado</span></td>
                          <td>
                            <a class="btn btn-app bg-info">
                              <i class="fas fa-list"></i> Ver
                            </a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.table-responsive -->
                </div>

              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="settings">
                <form class="form-horizontal">
                  <div class="form-group row">
                    <label for="inputName" class="col-sm-2 col-form-label">Nombre</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" placeholder="Nombre">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail" class="col-sm-2 col-form-label">Apellido</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputApellido" placeholder="Apellido">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputName2" class="col-sm-2 col-form-label">Nombre de Usuario</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName2" placeholder="Nombre de Usuario">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputName2" class="col-sm-2 col-form-label">Correo</label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="inputName2" placeholder="Correo">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputName2" class="col-sm-2 col-form-label">DNI</label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" id="inputName2" placeholder="DNI">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputName2" class="col-sm-2 col-form-label">Celular</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName2" placeholder="Celular">
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox"> Aceptar <a href="#">terminos y servicios</a>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">GUARDAR</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div><!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>


@endsection
@section('script')
    
@endsection