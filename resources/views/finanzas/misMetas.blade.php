@extends('layouts.app')
@section('contenido')
<!-- Content Header (Mis Metas) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Mis Metas</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ Route('home') }}" class="text-dark" class="text-dark">Inicio</a></li>
            <li class="breadcrumb-item">Finanzas</li>
            <li class="breadcrumb-item active"><a href="{{ Route('misMetas') }}" class="text-dark" class="text-dark">Mis Metas</a></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
@endsection
@section('script')
    
@endsection