<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//use Kreait\Firebase\Auth as FirebaseAuth;
use Kreait\Firebase\Exception\FirebaseException;
use Illuminate\Validation\ValidationException;

//use Auth;
use Kreait\Firebase\Database;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Auth;
use Firebase\Auth\Token\Exception\InvalidToken;

class TiendaController extends Controller
{

    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $firebase = (new Factory)->withServiceAccount(__DIR__.'/firebaseService.json');

        $database = $firebase->createDatabase();

        $user = \Auth::user()->localId;

        $ref = $database->getReference("Negocio_Usuario/".$user);

        $negocios = $ref->getValue();

        if ($negocios == null) {

            $tiendas[] = ["id"=>"", "nombre"=>"", "imagen"=>"", "categoria"=>""];
            
        }else {
            foreach ($negocios as $n) {
    
                $tiendas[] = $n;
    
            }
        }



        return view('tienda.index', compact('tiendas'));
    }

    public function administrarTienda()
    {
        return view('tienda.administrar');
    }

    public function editarTienda()
    {
        return view('tienda.editar');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear(Request $request)
    {

        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
